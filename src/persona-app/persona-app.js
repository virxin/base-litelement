import {LitElement, html} from 'lit';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-stats/persona-stats.js';
import '../persona-dm/persona-dm.js';

class PersonaApp extends LitElement{

    static get properties() {
       return {
        people: {type: Array}
       };
    }

    constructor() {
        super();
        this.people = [];
    }

    updated(changedProperties) {
        console.log("updated en persona-app");
        if(changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en persona-app");
            this.shadowRoot.querySelector("persona-stats").people = this.people;
        }
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <persona-header></persona-header>
            <div class="row">
                <persona-sidebar class="col-2" @new-person-event="${this.newPerson}" @value-range-event="${this.valueRange}"></persona-sidebar>
                <persona-main class="col-10" @people-updated="${this.peopleUpdated}"></persona-main>
            </div>
            <persona-footer></persona-footer>
            <persona-stats
                @updated-people-stats="${this.updatedPeopleStats}"
            >
            </persona-stats>
            <persona-dm
                @person-data="${this.personData}"
            >
            </persona-dm>
        `
    }

    newPerson(e) {
        console.log("newPerson en persona-app");
        this.shadowRoot.querySelector("persona-main").showPersonForm = true;
    }

    updatedPeopleStats(ev) {
        console.log("updatedPeopleStats en persona-app");
        console.log(ev.detail);
        this.shadowRoot.querySelector("persona-sidebar").peopleStats = ev.detail.peopleStats;
    }

    peopleUpdated(ev) {
        console.log("peopleUpdated en persona-app");
        console.log(ev.detail);
        this.people = ev.detail;
    }

    personData(ev) {
        console.log("personData en persona-app");
        console.log(ev.detail);
        this.shadowRoot.querySelector("persona-main").people = ev.detail;
    }

    valueRange(ev) {
        console.log("valueRange en persona-app");
        console.log(ev.detail);
    }
}

customElements.define("persona-app", PersonaApp);