import {LitElement, html} from 'lit';

class PersonaDm extends LitElement{

    static get properties() {
       return {
            people: {type: Array}
       };
    }

    updated(changedProperties) {
        console.log("Updated en persona-dm");
        if(changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en persona-dm");
            this.dispatchEvent(
                new CustomEvent(
                    "person-data",
                    {
                        detail: this.people
                    }
                )
            );
        }
    }

    constructor() {
        super();
        this.people = [
            {
                name: "Jimi Hendrix",
                yearsInCompany: 10,
                photo: {
                    src: "./img/jimi.jpg",
                    alt: "Jimi Hendrix"
                },
                profile: "Guitarrista uno"
            }, {
                name: "Eric Clapton",
                yearsInCompany: 3,
                photo: {
                    src: "./img/eric.jpg",
                    alt: "Eric Clapton"
                },
                profile: "Guitarrista dos"
            },{
                name: "Jimmy Page",
                yearsInCompany: 5,
                photo: {
                    src: "./img/jimmy.jpg",
                    alt: "Jimmy Page"
                },
                profile: "Guitarrista tres"
            },
            {
                name: "Eddie Van Halen",
                yearsInCompany: 9,
                photo: {
                    src: "./img/eddie.jpg",
                    alt: "Eddie Van Halen"
                },
                profile: "Guitarrista cuatro"
            },{
                name: "B. B. King",
                yearsInCompany: 1,
                photo: {
                    src: "./img/bbking.jpg",
                    alt: "B. B. King"
                },
                profile: "Guitarrista cinco"
            }
        ];
    }
}

customElements.define("persona-dm", PersonaDm);