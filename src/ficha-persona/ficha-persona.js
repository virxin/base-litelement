import {LitElement, html} from 'lit';

class FichaPersona extends LitElement{
    
    static get properties() {
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String}        }
    }
    
    constructor() {
        super();
        this.name = "Virginia";
        this.yearsInCompany = "12";
    }

    //Cuando se actualizan las propiedades y se han pintado en el DOM
    //changedProperties -> tipo map
    updated(changedProperties) {
        changedProperties.forEach(
             /*mapeo(oldValue, propName) {
                console.log("Propiedad " + propName + " ha cambiado de valor, anterior era " + oldValue);
            }*/
            //landa, función reducida de la anterior
            (oldValue, propName) => {
                console.log("Propiedad " + propName + " ha cambiado de valor, anterior era " + oldValue);
            }
        )
        //acceder a una propiedad cuando cambia
        if (changedProperties.has("name")) {
            console.log("Propiedad name cambia valor, anterior era " + changedProperties.get("name") + " nuevo es " + this.name);
        }
        if (changedProperties.has("yearsInCompany")) {
            console.log("Propiedad name cambia valor, anterior era " + changedProperties.get("yearsInCompany") + " nuevo es " + this.yearsInCompany);
            this.updatePersonInfo();
        }
    }

    render() {
        //Input:
            //@change: se ejecuta cuando se pierde el foco y se ha modificado el valor
            //@input: Cada vez que se cambia el valor no hace falta q se pieda el foco
        return html`
            <div>
                <label>Nombre Completo</label>
                <input type="text" id"fname" value="${this.name}" @input="${this.updateName}"></input>
                <br />
                <label>Años en la empresa</label>
                <input type="text" id"fAniosEmpresa" value="${this.yearsInCompany}"  @input="${this.updateYearsInCompany}"></input>
                <br />
                <input type="text" id"fPeronInfo" value="${this.personInfo}" disabled></input>
                <br />
            </div>
        `
    }   

    updateName(e) {
        console.log("updateName");
        //console.log(e);
        this.name = e.target.value;
    }

    updateYearsInCompany(e) {
        console.log("updateYearsInCompany");
        this.yearsInCompany = e.target.value;
    }

    updatePersonInfo() {
        console.log("updatePersonInfo");
        if(this.yearsInCompany >= 7) {
            this.personInfo = "lead";
        } else if(this.yearsInCompany >= 5) {
            this.personInfo = "senior";
        } else if(this.yearsInCompany >= 3) {
            this.personInfo = "team";
        } else {
            this.personInfo = "junior";
        }
    }
}

customElements.define("ficha-persona", FichaPersona);