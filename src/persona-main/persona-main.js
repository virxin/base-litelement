import {LitElement, html} from 'lit';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';

class PersonaMain extends LitElement{

    static get properties() {
       return {
            people: {type: Array},
            showPersonForm: {type: Boolean}
       };
    }

    constructor() {
        super();
        this.people = [];
        this.showPersonForm = false;
    }

    updated(changedProperties) {
        console.log("updated en persona-main");
        if (changedProperties.has("showPersonForm")) {
            console.log("Ha cambiado el valor de la propiedad showPersonForm en perona-main");
            if(this.showPersonForm === true) {
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        }

        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en perona-main");
            this.dispatchEvent(
                new CustomEvent(
                    "people-updated",
                    {
                        detail: this.people

                    }
                )
            );
        }
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <main class="row -cols-1 row-cols-sm-4">
                    ${this.people.map(
                        person => html`<persona-ficha-listado
                                            fname="${person.name}"
                                            yearsInCompany="${person.yearsInCompany}"
                                            .photo="${person.photo}"
                                            profile="${person.profile}"
                                            @delete-person-event="${this.deletePerson}"
                                            @more-info-person-event="${this.moreInfo}"
                                        >
                                        </persona-ficha-listado>`
                    )}
                </main>
            </div>
            <div class="row">
                <persona-form
                    id="personForm"
                    class="d-none border rounded border-primary"
                    @close-person-form-event="${this.personFormClose}"
                    @save-person-form-event="${this.personFormSave}"
                >
                </persona-form>
            </div>
        `
    }

    deletePerson(e) {
        console.log("deletePerson en persona-main");
        console.log("Se va a borrar la persona de nombre " + e.detail.name);
        this.people = this.people.filter(
            person => person.name != e.detail.name
        );
    }

    showPersonList() {
        console.log("showPersonList");
        console.log("Mostrando el listado de personas");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
    }

    showPersonFormData() {
        console.log("showPersonFormData");
        console.log("Mostrando el formulario de personas");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }

    personFormClose() {
        console.log("personFormClose");
        this.showPersonForm = false;
    }

    personFormSave(e) {
        console.log("personFormSave");
        console.log(e.detail.person);
        if(e.detail.editingPerson === true) {
            console.log("Se va a actualizar la persona de nombre: " + e.detail.person.name);
            this.people = this.people.map(
                person => person.name === e.detail.person.name
                    ? person =  e.detail.person : person
            );
        } else {
            console.log("Se va a almacenar una persona nueva");
            //JS spread syntax
            this.people = [...this.people, e.detail.person];
        }
        console.log("Proceso terminado");
        this.showPersonForm = false;
    }

    moreInfo(e) {
        console.log("moreInfo en persona-main");
        console.log("Se va a ver la info de la persona de nombre " + e.detail.name);
        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        let person = {};
        person.name = chosenPerson[0].name;
        person.profile = chosenPerson[0].profile;
        person.yearsInCompany = chosenPerson[0].yearsInCompany;
        person.photo = chosenPerson[0].photo;

        this.shadowRoot.getElementById("personForm").person = person;
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;
    }
}

customElements.define("persona-main", PersonaMain);