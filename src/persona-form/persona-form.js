import {LitElement, html} from 'lit';

class PersonaForm extends LitElement{

    static get properties() {
       return {
            person: {type: Object},
            editingPerson: {type: Boolean}
       };
    }

    constructor() {
        super();
        this.resetFormData();
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <div>
                <form>
                    <div class="form-group">
                        <label>Nombre completo</label>
                        <input
                            type="text"
                            class="form-control"
                            placeholder="Nombre completo"
                            .value="${this.person.name}"
                            @input="${this.updateName}"
                            ?disabled="${this.editingPerson}"
                        >
                        </input>
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea
                            class="form-control"
                            placeholder="Perfil"
                            rows="5"
                            .value="${this.person.profile}"
                            @input="${this.updateProfile}"
                        >
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input
                            type="text"
                            class="form-control"
                            placeholder="Años en la empresa"
                            .value="${this.person.yearsInCompany}"
                            @input="${this.updateYearsInCompany}"
                        >
                        </input>
                    </div>
                    <button class="btn btn-primary" @click="${this.goBack}"><strong>Atrás<strong></button>
                    <button class="btn btn-success" @click="${this.storePerson}"><strong>Guardar<strong></button>
                </form>
            </div>
        `
    }

    updateName(e) {
        console.log("updateName en persona-form");
        console.log("Actualizando la propiedad name con el valor: " + e.target.value);
        this.person.name = e.target.value;
    }

    updateProfile(e) {
        console.log("updateProfile en persona-form");
        console.log("Actualizando la propiedad profile con el valor: " + e.target.value);
        this.person.profile = e.target.value;
    }

    updateYearsInCompany(e) {
        console.log("updateYearsInCompany en persona-form");
        console.log("Actualizando la propiedad years in company con el valor: " + e.target.value);
        this.person.yearsInCompany = e.target.value;
    }

    goBack(e) {
        console.log("goBack en persona-form");
        console.log("Se ha pulsado el botón guardar persona-form");
        e.preventDefault();
        this.dispatchEvent(
            new CustomEvent(
                "close-person-form-event",
                {}
            )
        );
        this.resetFormData();
        this.editingPerson = false;
    }

    storePerson(e) {
        console.log("storePerson en persona-form");
        console.log("Se ha pulsado el botón atrás persona-form");
        e.preventDefault();
        console.log("La propiedad name vale: " + this.person.name);
        console.log("La propiedad profile vale: " + this.person.profile);
        console.log("La propiedad yearsInCompany vale: " + this.person.yearsInCompany);
        this.person.photo = {
            src: "./img/fender.jpg",
            alt: this.person.name
        };
        this.dispatchEvent(
            new CustomEvent(
                "save-person-form-event",
                {
                    detail: {
                        person: {
                            name: this.person.name,
                            profile: this.person.profile,
                            yearsInCompany: this.person.yearsInCompany,
                            photo: this.person.photo
                        },
                        editingPerson: this.editingPerson
                    }
                }
            )
        );
    }

    resetFormData() {
        console.log("resetFormData");
        this.person = {};
        this.person.name = "";
        this.person.profile = "";
        this.person.yearsInCompany = "";
    }
}

customElements.define("persona-form", PersonaForm);